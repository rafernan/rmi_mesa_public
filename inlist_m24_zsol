! inlist to evolve a 12 solar mass star with Fuller+15 parameters

&star_job

    change_v_flag = .true.
    new_v_flag = .true.

    kappa_file_prefix = 'gs98'

    pgstar_flag = .false.

/ !end of star_job namelist


&controls

  xa_central_lower_limit_species(1) = 'h1'
  xa_central_lower_limit(1) = 1e-3
  profile_interval = 20
  history_interval = 1

  !Fuller+15 controls
    initial_mass = 24.
    initial_z    = 2d-2
    Zbase        = 2d-2

    sig_min_factor_for_high_Tcenter = 0.01
    Tcenter_min_for_sig_min_factor_full_on  = 3.2d9     
    Tcenter_max_for_sig_min_factor_full_off = 2.8d9     
    logT_max_for_standard_mesh_delta_coeff = 9.0     
    logT_min_for_highT_mesh_delta_coeff = 10.0     
    delta_Ye_highT_limit = 1d-3     
    okay_to_reduce_gradT_excess = .true.     
    allow_thermohaline_mixing = .true.     
    thermo_haline_coeff = 2.0     

    overshoot_f_above_nonburn = 0.035     
    overshoot_f_below_nonburn = 0.01     
    overshoot_f_above_burn_h = 0.035     
    overshoot_f_below_burn_h = 0.0035     
    overshoot_f_above_burn_he = 0.035     
    overshoot_f_below_burn_he = 0.0035     
    overshoot_f_above_burn_z = 0.035     
    overshoot_f_below_burn_z = 0.0035 

    RGB_wind_scheme = 'Dutch'
    AGB_wind_scheme = 'Dutch'
    RGB_to_AGB_wind_switch = 1d-4     
    Dutch_wind_eta = 0.8     
    include_dmu_dt_in_eps_grav = .true.      

    use_Type2_opacities = .false.     
    newton_itermin = 2     
    mixing_length_alpha = 1.5
    MLT_option = 'Henyey'  
    allow_semiconvective_mixing = .true.     
    alpha_semiconvection = 0.01     
    mesh_delta_coeff = 1.     
    varcontrol_target = 5d-4     
    max_allowed_nz = 10000     
    mesh_dlog_pp_dlogP_extra = 0.4     
    mesh_dlog_cno_dlogP_extra = 0.4     
    mesh_dlog_burn_n_dlogP_extra = 0.4     
    mesh_dlog_3alf_dlogP_extra = 0.4     
    mesh_dlog_burn_c_dlogP_extra = 0.10     
    mesh_dlog_cc_dlogP_extra = 0.10     
    mesh_dlog_co_dlogP_extra = 0.10     
    mesh_dlog_oo_dlogP_extra = 0.10     

    velocity_logT_lower_bound=9     
    dX_nuc_drop_limit = 5d-3     
    dX_nuc_drop_limit_at_high_T = 5d-3
    screening_mode = 'extended'
    max_iter_for_resid_tol1 = 3     
    tol_residual_norm1 = 1d-5     
    tol_max_residual1 = 1d-2     
    max_iter_for_resid_tol2 = 12     
    tol_residual_norm2 = 1d99
    tol_max_residual2 = 1d99     
    min_timestep_limit = 1d-12 ! (seconds)     
    delta_lgL_He_limit = 0.1 !     
    dX_nuc_drop_max_A_limit = 52     
    dX_nuc_drop_min_X_limit = 1d-4     
    dX_nuc_drop_hard_limit = 1d99     
    delta_lgTeff_limit = 0.5     
    delta_lgL_limit = 0.5     
    delta_lgRho_cntr_limit = 0.02     
    T_mix_limit = 0

    Pextra_factor = -1

/ ! end of controls namelist
