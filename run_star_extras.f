! ***********************************************************************
!
!   Copyright (C) 2010  Bill Paxton
!
!   this file is part of mesa.
!
!   mesa is free software; you can redistribute it and/or modify
!   it under the terms of the gnu general library public license as published
!   by the free software foundation; either version 2 of the license, or
!   (at your option) any later version.
!
!   mesa is distributed in the hope that it will be useful, 
!   but without any warranty; without even the implied warranty of
!   merchantability or fitness for a particular purpose.  see the
!   gnu library general public license for more details.
!
!   you should have received a copy of the gnu library general public license
!   along with this software; if not, write to the free software
!   foundation, inc., 59 temple place, suite 330, boston, ma 02111-1307 usa
!
! ***********************************************************************
 
      module run_star_extras

      use star_lib
      use star_def
      use const_def
      
      implicit none
      
      ! these routines are called by the standard run_star check_model
      contains
      
      include 'standard_run_star_extras_modified.inc'

      integer function how_many_extra_history_columns(s, id, id_extra)
         type (star_info), pointer :: s
         integer, intent(in) :: id, id_extra

         !set this number to the number of extra columns needed
         how_many_extra_history_columns = 31

      end function how_many_extra_history_columns


      subroutine data_for_extra_history_columns(s, id, id_extra, n, names, vals, ierr)
         type (star_info), pointer :: s
         integer, intent(in) :: id, id_extra, n
         character (len=maxlen_history_column_name) :: names(n)
         real(dp) :: vals(n)
         integer, intent(out) :: ierr

         !note: do NOT add these names to history_columns.list
         ! the history_columns.list is only for the built-in log column options.
         ! it must not include the new column names you are adding here.

         integer                    :: k, j, nz, iname, counterslow, counterfast
         real(dp)                   :: B, M, c, a, LSun, RSun, del_m, del_T4, kedge, kappa_t, &
                                       mcutoff
         real(dp), dimension(s% nz) :: erad, Xaxis, density, Hp, l, E, kappa, F, Fedd, eps, &
                                       game, wth, kk, wdiff, va, ci, cis, vas, vslow, wslow, &
                                       vfast, wfast, checkdiffslow,checkdifffast,checkthslow, &
                                       checkthfast, zi, instabilityslow, instabilityfast, var1, &
                                       var2, growthslow, mass, logRho, pressure, grav, dr, &
                                       pressure_scale_height, temperature, x, opacity, &
                                       luminosity_rad, radius, energy, unstablemassslow, &
                                       unstablemassfast, unstableradiusslow, unstableradiusfast, &
                                       unstabletauslow, unstabletaufast, eint

         integer                    :: conv_he_out, conv_he_in, conv_fe_out, conv_fe_in, &
                                       conv_surf, ind_subhp, ind_cave
         real(dp)                   :: interp_scalar, cis_rmhp, rad_rmhp, prpg_rmhp, pmpg_rmhp, &
                                       rho_rmhp, theta_rmhp, kap_rmhp
         real(dp)                   :: rho_cave, vcon_cave, mach_cave

         !how to get variables from pointer s, scalar and vector
         nz = s% nz
         B = 1000.
         mcutoff = 1d-9  !cutoff vcon/csound for convective zone boundaries

         kappa_t = 6.652459d-1/1.672622d0

         do k=nz,1,-1
           mass(k)    = s% m(k)
           Xaxis(k)   = 1d0 - ((s% m(k))/(s% m(1)))
           density(k) = s% rho(k)
           dr(k)      = (s% dr_div_csound(k))*(s% csound(k))
           temperature(k) = s% T(k)
           E(k)       = 3d0*(s% Prad(k))
          
           !radiative luminosity at outer edge of cell (matches that in profile) 
           if (k.gt.1) then
             kedge  = s%opacity(k) + s%dq(k)/(s%dq(k-1)+s%dq(k))*(s%opacity(k-1)-s%opacity(k))
             del_T4 = (s%Prad(k-1)-s%Prad(k))/s%dm_bar(k)          
             luminosity_rad(k) = -(s% area(k) * s% area(k))*clight/kedge*del_T4

           else
             luminosity_rad(1) = luminosity_rad(2)

           endif

           F(k)     = luminosity_rad(k)/s% area(k)
           Fedd(k)  = clight*(s% grav(k))/(s% opacity(k))
           eps(k)   = F(k)/Fedd(k)                              !matlab variable is epsilon
           eint(k)  = dexp(s% lnE(k)) - E(k)/density(k)         !subtract radiation energy 
           game(k)  = 1d0 + s% Pgas(k)/(density(k)*eint(k))
           kappa(k) = s% opacity(k) - 0.5d0*kappa_t*(1d0 + (s% X(k)))

           Hp(k)      = (s% Pgas(k))/(s%rho(k)*s%grav(k)*(1d0 - eps(k))) !gas pressure scale height
           l(k)       = dr(k)/Hp(k)

           wth(k)   = 4d0*clight*(game(k)-1d0)*kappa(k)*density(k)*E(k)/(s% Pgas(k))
           kk(k)    = 1d0/Hp(k)
           wdiff(k) = (clight*kk(k)*kk(k))/(3d0*(s% opacity(k))*density(k))
           va(k)    = B/SQRT(4.*pi*density(k))
           ci(k)    = SQRT((s% Pgas(k))/density(k))
           cis(k)   = ci(k)*ci(k)
           vas(k)   = va(k)*va(k)

           vslow(k) = SQRT(0.5*(cis(k) + vas(k) - SQRT((cis(k) + vas(k))**2 - 2*vas(k) * cis(k))))
           wslow(k) = kk(k)*vslow(k)
           vfast(k) = SQRT(0.5*(cis(k) + vas(k) + SQRT((cis(k) + vas(k))**2 - 2*vas(k) * cis(k))))
           wfast(k) = kk(k)*vfast(k)

           ! checks for w_slow, w_diff, w_th
           checkdiffslow(k) = wdiff(k)/wslow(k)
           checkdifffast(k) = wdiff(k)/wfast(k)
           checkthslow(k) = wth(k)/wslow(k)
           checkthfast(k) = wth(k)/wfast(k)
         
           zi(k) = MIN(va(k)/ci(k),1d0);
           instabilityslow(k) = F(k)/(zi(k)**3*(s%pgas(k) + 4d0*E(k)/3d0)*ci(k))
           instabilityfast(k) = (F(k)*zi(k)**3)/(va(k)*(s%pgas(k) + 4d0*E(k)/3d0))
         
           ! variables for growth rate calculation
           var1(k) = (eps(k)*(s% grav(k)))/(4.*ci(k))
           var2(k) = 1.+(3.*(s% pgas(k)))/(E(k)*4.)
           growthslow(k) = var1(k)*var2(k)*zi(k)*(1.-1./instabilityslow(k))

         enddo

         !find innermost points where instability operates
         counterslow = 1; counterfast = 1
         do k=1,nz         ! start from the surface

           if (checkdiffslow(k) .ge. 1d0 .and. instabilityslow(k) .gt. 1d0) THEN    
            counterslow           = k                                 !cell index
            unstablemassslow(k)   = 1d0 - s%m(k)/s%m(1)               !fractional exterior mass
            unstableradiusslow(k) = (s% r(1) - s% r(k))/rsol          !radius from the surface, in Rsun
            unstabletauslow(k)    = s% tau(k)                         !optical depth
           end if

           if (checkdifffast(k) .ge. 1d0 .and. instabilityfast(k) .gt. 1d0) THEN
            counterfast           = k
            unstablemassfast(k)   = 1d0 - s%m(k)/s%m(1)
            unstableradiusfast(k) = (s%  r(1) - s% r(k))/rsol
            unstabletaufast(k)    = s% tau(k)
           end if

         enddo

         !Find He and Fe convection zones

         conv_he_out = 0; conv_he_in = 0   !indices
         conv_fe_out = 0; conv_fe_in = 0

         conv_surf = 0 
         if (s%conv_vel(1)/s%csound(1).gt.mcutoff) conv_surf = 1

         !index of outer edge of He convection zone
         if (conv_he_out.eq.0) then
           do k=1,nz
             if (s%conv_vel(k)/s%csound(k).gt.mcutoff .and. s%T(k).gt.2.5e+4 .and. s%T(k).lt.1e+5) then
               conv_he_out = k
               exit             !break the cycle if vconv > 0 (convection zone)

             endif         
           enddo
         endif

         !index of inner edge of He convection zone
         if (conv_he_out.gt.0) then
           do k=conv_he_out+1,nz
             if (s%conv_vel(k)/s%csound(k).lt.mcutoff) then
               conv_he_in = k-1
               exit

             endif
           enddo
         endif

         !include case of no He convection zone
         if (conv_he_out.eq.0) then
           conv_he_in = 1
         endif

         !index of outer edge of Fe convection zone
         if (conv_he_in.gt.0) then
           do k=conv_he_in,nz
             if (s%conv_vel(k)/s%csound(k).gt.mcutoff .and. s%T(k).gt.1e+5 .and. s%T(k).lt.4e+5) then
               conv_fe_out = k
               exit

             endif
           enddo
         endif

         !index of inner edge of Fe convection zone
         if (conv_fe_out.gt.0) then
           do k=conv_fe_out+1,nz
             if (s%conv_vel(k)/s%csound(k).lt.mcutoff) then
               conv_fe_in = k-1
               exit

             endif
           enddo
         endif

         !compute average convective quantities averaged over top 1.5Hp
         !  of FeCZ
         rho_cave=0.; vcon_cave=0.; mach_cave=0.
         if (conv_fe_out.gt.0) then
           !find index of r(conv_fe_out)-Hp
           do k=conv_fe_out,nz
             if (s%rmid(k).lt.(s%rmid(conv_fe_out)-1.5*s%scale_height(conv_fe_out))) then
               ind_cave = k
               exit

             endif
           enddo           

           !compute average quantities 
           !!rho_cave = sum(s%dm(conv_fe_out:ind_cave))
           rho_cave  = sum(s%dm(conv_fe_out:ind_cave)*s%rho(conv_fe_out:ind_cave))/sum(s%dm(conv_fe_out:ind_cave)) 
           vcon_cave = sum(s%dm(conv_fe_out:ind_cave)*s%conv_vel(conv_fe_out:ind_cave))/sum(s%dm(conv_fe_out:ind_cave)) 
           mach_cave = sum(s%dm(conv_fe_out:ind_cave)*s%conv_vel(conv_fe_out:ind_cave)/s%csound(conv_fe_out:ind_cave))&
                          /sum(s%dm(conv_fe_out:ind_cave)) 

         endif

         !print *, 'conv indices =', conv_he_out, conv_he_in, conv_fe_out, conv_fe_in

         !find index of tau = 10 to evaluate sub-surface quantities 
         do k=1,nz-1
           !if (s%r(k).gt.(s%r(1)-Hp(1)) .and. s%r(k+1).le.(s%r(1)-Hp(1))) then
           if (s%tau(k).lt.10d0 .and. s%tau(k+1).ge.10d0) then
             ind_subhp = k
             exit
           endif
         enddo
         !R-Hp (too shallow, tau ~ 2, diffusion approximation not quite valid)
           !print *, 'surf index =', s%r(ind_subhp), s%r(1)-Hp(1), s%r(ind_subhp+1)
           !interp_scalar = (s%r(1)-Hp(1)-s%r(ind_subhp))/(s%r(ind_subhp+1)-s%r(ind_subhp))
           !cis_rmhp = cis(ind_subhp) + interp_scalar*(cis(ind_subhp+1)-cis(ind_subhp))
           !tau_rmhp = s%tau(ind_subhp) + interp_scalar*(s%tau(ind_subhp+1)-s%tau(ind_subhp))

         !tau = 10
           interp_scalar = (10d0-s%tau(ind_subhp))/(s%tau(ind_subhp+1)-s%tau(ind_subhp))
           !(R-r)/rsol at tau = 10
           rad_rmhp = s%r(ind_subhp) + interp_scalar*(s%r(ind_subhp+1)-s%r(ind_subhp))
           rad_rmhp = (s%r(1)-rad_rmhp)/rsol
           !isothermal sound speed
           cis_rmhp = cis(ind_subhp) + interp_scalar*(cis(ind_subhp+1)-cis(ind_subhp))
           !pressure ratios
           prpg_rmhp = s%Prad(ind_subhp)/s%Pgas(ind_subhp) + &
                         interp_scalar*(s%Prad(ind_subhp+1)/s%Pgas(ind_subhp+1)-s%Prad(ind_subhp)/s%Pgas(ind_subhp))
           pmpg_rmhp = s%Pgas(ind_subhp) + interp_scalar*(s%Pgas(ind_subhp+1)-s%Pgas(ind_subhp))
           pmpg_rmhp = B**2/(8d0*pi)/pmpg_rmhp
           !density (for va)
           rho_rmhp = s%rho(ind_subhp) + interp_scalar*(s%rho(ind_subhp+1)-s%rho(ind_subhp))
           !logarithmic opacity derivative
           theta_rmhp = s%d_opacity_dlnd(ind_subhp) + interp_scalar*(s%d_opacity_dlnd(ind_subhp+1) - s%d_opacity_dlnd(ind_subhp)) 
           kap_rmhp   = s%opacity(ind_subhp)        + interp_scalar*(s%opacity(ind_subhp+1) - s%opacity(ind_subhp))
           theta_rmhp = theta_rmhp/kap_rmhp

         !OUTPUT

         vals(:) = 0.

         names(1) = 'erad (test)'    !name of the extra column (1..n)
         vals(1)  = erad(nz)         !value of extra column (scalar, 1..n)
        
         !innermost cell for instability 
         names(2) = 'unstablemassslow'
         vals(2)  = unstablemassslow(counterslow)  
 
         names(3) = 'unstableradiusslow'
         vals(3)  = unstableradiusslow(counterslow)

         names(4) = 'unstabletauslow'
         vals(4)  = unstabletauslow(counterslow)

         names(5) = 'unstablemassfast'
         vals(5)  = unstablemassfast(counterfast)

         names(6) = 'unstableradiusfast'
         vals(6)  = unstableradiusfast(counterfast)

         names(7) = 'unstabletaufast'
         vals(7)  = unstabletaufast(counterfast)

         !pressure ratios at the surface
         names(8) = 'prad/pgas surface'
         vals(8)  = s%Prad(1)/s%Pgas(1)

         names(9) = 'pmag/pgas surface'
         vals(9)  = B**2/(8d0*pi)/s%Pgas(1)

         !convection zones
         names(10) = 'conv_he_out mass'
         if (conv_he_out.gt.0) &
           vals(10)  = 1d0 - s%m(conv_he_out)/s%m(1)

         names(11) = 'conv_he_in mass'
         if (conv_he_out.gt.0) &
           vals(11)  = 1d0 - s%m(conv_he_in)/s%m(1)

         names(12) = 'conv_fe_out mass'
         if (conv_fe_out.gt.0) &
           vals(12)  = 1d0 - s%m(conv_fe_out)/s%m(1)

         names(13) = 'conv_fe_in mass'
         if (conv_fe_out.gt.0) &
           vals(13)  = 1d0 - s%m(conv_fe_in)/s%m(1)

         names(14) = 'conv_he_out radius'
         if (conv_he_out.gt.0) &
           vals(14)  = (s%r(1) - s%r(conv_he_out))/rsol

         names(15) = 'conv_he_in radius'
         if (conv_he_out.gt.0) &
           vals(15)  = (s%r(1) - s%r(conv_he_in))/rsol

         names(16) = 'conv_fe_out radius'
         if (conv_fe_out.gt.0) &
           vals(16)  = (s%r(1) - s%r(conv_fe_out))/rsol

         names(17) = 'conv_fe_in radius'
         if (conv_fe_out.gt.0) &
           vals(17)  = (s%r(1) - s%r(conv_fe_in))/rsol

         names(18) = 'surf conv mach'
         vals(18)  = conv_surf*s%conv_vel(1)/s%csound(1)

         names(19) = 'Hgas(R)/Rsun'
         vals(19)  = Hp(1)/rsol

         names(20) = '(R-r[tau=10])/rsol'
         vals(20)  = rad_rmhp

         names(21) = 'ci^2(R)'
         vals(21)  = cis(1)

         names(22) = 'ci^2(tau=10)'
         vals(22)  = cis_rmhp

         names(23) = 'pr/pg (tau=10)'
         vals(23)  = prpg_rmhp

         names(24) = 'pm/pg (tau=10)'
         vals(24)  = pmpg_rmhp

         names(25) = 'dens (tau=10)'
         vals(25)  = rho_rmhp

         names(26) = 'Theta_rho (tau=10)'
         vals(26)  = theta_rmhp

         names(27) = 'rho_cave'
         vals(27)  = rho_cave

         names(28) = 'vcon_cave'
         vals(28)  = vcon_cave

         names(29) = 'mach_cave'
         vals(29)  = mach_cave

         names(30) = 'ind_cave'
         vals(30)  = ind_cave

         names(31) = 'conv_fe_out'
         vals(31)  = conv_fe_out

         ierr = 0
         return
      end subroutine data_for_extra_history_columns

!      integer function how_many_extra_profile_columns(id, id_extra)
!         use star_def, only: star_info
!         integer, intent(in) :: id, id_extra
!         integer :: ierr
!         type (star_info), pointer :: s
!         ierr = 0
!         call star_ptr(id, s, ierr)
!         if (ierr /= 0) return
!         how_many_extra_profile_columns = 26
!      end function how_many_extra_profile_columns
!
!     subroutine data_for_extra_profile_columns(id, id_extra, n, nz, names, vals, ierr)
!         use star_def, only: star_info, maxlen_profile_column_name
!         use const_def, only: dp
!         integer, intent(in) :: id, id_extra, n, nz
!         character (len=maxlen_profile_column_name) :: names(n)
!         real(dp) :: vals(nz,n)
!         integer, intent(out) :: ierr
!         type (star_info), pointer :: s
!         integer :: k, B, a, c, LSun, RSun,
!         ierr = 0
!         call star_ptr(id, s, ierr)
!         if (ierr /= 0) return

         ! note: do NOT add the extra names to profile_columns.list
         ! the profile_columns.list is only for the built-in profile column
         ! options.
         ! it must not include the new column names you are adding here.
 
         ! here is an example for adding a profile column
!         if (n /= 1) stop 'data_for_extra_profile_columns'
!          names(1) = 'erad'
!          names(2) = 'Xaxis'
!          names(3) = 'density'
!          names(4) = 'Hp'
!          names(5) = 'l'
!          names(6) = 'E'
!          names(7) = 'kappa'
!          names(8) = 'F'
!          names(9) = 'eps'
!          names(10) = 'game'
!          names(11) = 'wth'
!          names(12) = 'kk'
!          names(13) = 'wdiff'
!          names(14) = 'va'
!          names(15) = 'ci'
!          names(16) = 'vslow'
!          names(17) = 'wslow'
!          names(18) = 'vfast'
!          names(19) = 'wfast'
!          names(20) = 'checkdiffslow'
!          names(21) = 'checkdifffast'
!          names(22) = 'checkthslow'
!          names(23) = 'checkthfast'
!          names(24) = 'instabilityslow'
!          names(25) = 'instabilityfast'
!          names(26) = 'growthslow'
          
!          nz = s% nz
!          B = 1000
!          c = 2.99792458e+10
!          a = 7.5657*(10**15)
!          LSun = 3.839*(10**33)
!          RSun = 6.955*(10**10)
          

!         do k = 1, nz
           
!           vals(k,1) = 3*(s% prad(k))
!           vals(k,2) = 1-(s% m(k))/(s% m(1))
!          vals(k,3) = 10**(s% lnd(k))
!           vals(k,4) = (s% P(k))/((s% rho(k))*(s%grav(k)))
!           dr = (s% dr_div_csound(k))*(s% csound(k))
!           pressure_scale_height = (s% P(k))/(density(k) * s% grav(k))
!           vals(k,5) = dr/((pressure_scale_height)*6.955*(10**10))
!           vals(k,6) = a*(s% T(k))**4
!           vals(k,7) = (s% opacity(k)) - 0.2*(1+(s% x(k)))
!
!           if (k == 1) then
!            j = 2
!           else
!            j = k
!           end if
!            del_m = 0.5d0*(s% dm(j-1) + s% dm(j))
!            del_T4 = (s% T(j-1))**4 - (s% T(j))**4
!            luminosity_rad = -s% area(j)*s%area(j)*crad*c/(3*s% opacity(k))*(del_T4/del_m)
!           
!           vals(k,8) = (LSun*(luminosity_rad))/(4*pi*RSun*RSun*(s% R(k))*(s% R(k)))
!           Fedd = (c*(s% grav(k)))/(s% opacity(k))
!           vals(k,9) = (F(k))/(Fedd) !matlab variable is epsilon
!           density(k) = 10**(s% lnd(k))
!           vals(k,10) = 1 + (s% P(k))/((density(k))*(10**(s% lnE(k))))
!           kappa = (s% opacity(k)) - 0.2*(1+(s% x(k)))
!           vals(k,11) = 4*a*c*density*kappa*(game(k)-1)*((s% temperature(k))**4)/(s% pgas(k))
!           vals(k,12) = 1/((pressure_scale_height(k))*RSun)
!           vals(k,13) = (c*kk(k)*kk(k))/(3*(s% opacity(k))*density(k))
!           vals(k,14) = B/SQRT(4*pi*density(k))
!           vals(k,15) = SQRT((s% pgas(k))/density(k))
!           vals(k,16) = SQRT(0.5*(cis(k) + vas(k) - SQRT((cis(k) + vas(k))**2 - 2*vas(k) * cis(k))))
!           vals(k,17) = kk(k)*SQRT(0.5*(cis(k) + vas(k) - SQRT((cis(k) + vas(k))**2 - 2*vas(k) * cis(k))))
!           vals(k,18) = SQRT(0.5*(cis(k) + vas(k) + SQRT((cis(k) + vas(k))**2 - 2*vas(k) * cis(k))))
!           vals(k,19) = kk(k)*SQRT(0.5*(cis(k) + vas(k) + SQRT((cis(k) + vas(k))**2-2*vas(k) * cis(k))))
!           vals(k,20) = wdiff(k)/wslow(k)
!           vals(k,21) = wdiff(k)/wfast(k)
!           vals(k,22) = wth(k)/wslow(k)
!           vals(k,23) = wth(k)/wfast(k)
!           vals(k,24) = F(k)/((zi(k)**3)*((s% pgas(k)) + 4*E(k)/3)*ci(k))
!           vals(k,25) = (F(k)*(zi(k)**3))/(va(k)*((s% pgas(k)) + 4*E(k)/3))
!           vals(k,26) = var1(k)*var2(k)*zi(k)*(1-1/instabilityslow(k))
!           
!          end do
!
!      end subroutine data_for_extra_profile_columns
!
      end module run_star_extras
      
